## Hello World
echo "Hello World"


#[ This is a multi line comment
it continues until it is terminated
]#

## Variables
#[
  Nim supports three different types of variables, let, var, and const.
  As with most things, multiple variables can be declared in the same section.
]#
proc getAlphabet(): string =
  var accm = ""
  for letter in 'a'..'z':  # see iterators
    accm.add(letter)
  return accm

# Computed at compilation time
const alphabet = getAlphabet()

# Mutable variables
var
  a = "foo"
  b = 0
  # Works fine, initialized to 0
  c: int

# Immutable variables
let
  d = "foo"
  e = 5
  # Compile-time error, must be initialized at creation
##  f: float

# Works fine, `a` is mutable
a.add("bar")
b += 1
c = 3

# Compile-time error, const cannot be modified at run-time
## alphabet = "abc"

# Compile-time error, `d` and `e` are immutable
## d.add("bar")
## e += 1


proc getAlphabet2(): string =
  for letter in 'a'..'z':
    result.add(letter)


## Type Casting and Inference
#[
Nim is a statically typed language.
As such, each variable has a type associated with it.
As seen in the previous example these types are inferred in the const, let and var declarations by the compiler.
]#
# These types are inferred.
var x = 5 # int
var y = "foo" # string

# Assigning a value of a different type will result in a compile-time error.
## x = y

#[
You may optionally specify the type after a colon (:).
In some cases the compiler will expect you to explicitly cast types, for which multiple ways are available:
]#
##  - type conversion, which is safety checked by the compiler
##  - annotating the variable type
##  - the cast keyword, which is unsafe and should be used only where you know what you are doing, such as in interfacing with C

var xx = int(1.0 / 3) # type conversion

var yy: seq[int] = @[] # empty seq needs type specification

var zz = "Foobar"
proc ffi(foo: ptr array[6, char]) = echo repr(foo)
ffi(cast[ptr array[6, char]](addr zz[0]))


## If, Else, While
#[
Nim has many different control flow constructs, including the standard [if]s, [else]s, and [while]s.
However, Nim does not use an [else if] construct like many languages, it uses a more condensed elif.
When inside a loop,
 - [continue] can be used to skip the rest of the loop body and to begin the next iteration;
 - [break] can be used to immediately leave the loop body.
Along with its other uses,
 - the [block] statement can be used to create a label so that it’s possible to break out of nested loops.
]#
import strutils, random

randomize()
let answer = rand(9) + 1
while true:
  echo "I have a number from 1 to 10, what is it? "
  let guess = answer
  if guess < answer:
    echo "Too low, try again"
  elif guess > answer:
    echo "Too high, try again"
  else:
    echo "Correct!"
    break

block busyloops:
  while true:
    while true:
      break busyloops


## Case Statements
#[
Nim also supports case statements, which are like switches in other languages.
There are several things to note here:
 -  You can use strings in the switch statement
 -  Sets and ranges of ordinal types are also usable
 -  case statements, like most things, are actually expressions
 -  It is required that every possible case be covered
]#
case "charlie":
  of "alfa":
    echo "A"
  of "bravo":
    echo "B"
  of "charlie":
    echo "C"
  else:
    echo "Unrecognized letter"

case 'h':
  of 'a', 'e', 'i', 'o', 'u':
    echo "Vowel"
  of '\127'..'\255':
    echo "Unknown"
  else:
    echo "Consonant"

proc positiveOrNegative(num: int): string =
  result = case num:
    of low(int).. -1:
      "negative"
    of 0:
      "zero"
    of 1..high(int):
      "positive"
    else:
      "impossible"

echo positiveOrNegative(-1)


## First Class Functions
#[
Nim supports closures as well as passing functions. Two different syntaxes available for closures:
    proc syntax, which is identical to regular procedure syntax
    “do notation”, which is a bit shorter
]#
import sequtils

let powersOfTwo = @[1, 2, 4, 8, 16, 32, 64, 128, 256]

echo(powersOfTwo.filter do (x: int) -> bool: x > 32)
echo powersOfTwo.filter(proc (x: int): bool = x > 32)

proc greaterThan32(x: int): bool = x > 32
echo powersOfTwo.filter(greaterThan32)

# The stdlib also makes a third option available by using macros:

import sugar

# sugar provides a "->" macro that simplifies writing type
# declarations, e.x. (char) -> char
proc map(str: string, fun: (char) -> char): string =
  for c in str:
    result &= fun(c)

# sugar also provides a "=>" macro for the actual lambda
# value
echo "foo".map((c) => char(ord(c) + 1))
# the following code is exactly equivalent:
echo "foo".map(proc (c: char): char = char(ord(c) + 1))


## Primitives
#[
Nim has several primitive types:
 - signed integers: [int8], [int16], [int32], [int64], and [int], where [int] is the same size as a pointer
 - unsigned integers are similar with u prepended to the type
 - floating points numbers: [float32], [float64], and [float], where float is the processor’s fastest type
 - characters: [char], which is basically an alias for uint8
To indicate the size of an integer literal, append u or i and the size you’d like to the end. However, usually this is not necessary.
Integers can also have 0[xX], 0o, 0[Bb] prepended to indicate a hex, octal, or binary literal, respectively. Underscores are also valid in literals, and can help with readability.
]#
let
  aa: int8 = 0x7F # Works
  bb: uint8 = 0b1111_1111 # Works
  dd = 0xFF # type is int
##  c: uint8 = 256 # Compile time error

#[
Precedence rules are the same as in most other languages,
but instead of
    [^],   [&],   [|],  [>>],  [<<],
the [xor], [and], [or], [shr], [shl] operators are used, respectively.
]#
let
  aaa: int = 2
  bbb: int = 4
echo 4/2


## Types
#[
Types are declared inside type sections, where multiple types can be declared.
Note that aliased types are the same, and not in any way incompatible with their original type.
If type safety is desired, distinct types should be used.
]#

type
  MyInteger* = int

let aba: int = 2
discard aba + MyInteger(4)

## Objects
#[
In Nim, objects are like structs from C family languages and define a grouping of fields.
They are by default traced by the garbage collector, so there is no need to explicitly free them when allocated.
]#

type
  Animal* = object
    name*, species*: string
    age: int

proc sleep*(ab: var Animal) =
  ab.age += 1

proc dead*(ac: Animal): bool =
  result = ac.age > 20

var carl: Animal
carl = Animal(name : "Carl",
              species : "L. glama",
              age : 12)

let joe = Animal(name : "Joe",
                 species : "H. sapiens",
                 age : 23)

assert(not carl.dead)
for i in 0..10:
  carl.sleep()
assert carl.dead

#[
Object types are declared in a type section, as usual.
They can be exported, and individual fields can also be exported.
Fields can be safely exported without violating encapsulation because call syntax is equivalent between them.
Initially, carl is created on the stack and initialized to zeros (or "" in case of fields of type string), so its value is [name = "", species = "", age = 0].
It is mutable, so that means that the contents of carl can be changed.
This also means it can be passed to functions that require a variable parameter, like sleep(), which can modify its value.
joe is also created on the stack, but it’s contents are immutable and can not be changed.
Attempting to do so, say through joe.age = 57, will fail with an error at compile time.
]#
let mittens: ref Animal = new(Animal)

mittens.name = "Mittens"
mittens.species = "P. leo"
mittens.age = 6

#[
[mittens] is a reference to an object allocated on the heap.
The value of [mittens] cannot be changed,
  so mittens can never point to anything else,
  but the value that mittens is pointing at can and is changed from the default initialization value of zeros.
You might ask whether there is a more concise way of initializing reference types, and there is if you give the reference type a name:
]#
type
  AnimalRef* = ref Animal

let spot = AnimalRef(name: "Spot",
                   species: "C. lupus",
                   age: 1)
#[
In many cases it is only wanted to have the object be a reference type, which is possible by declaring it as a ref object.
]#
type
  Thing* = ref object
    positionX*, positionY*: int


## Enums
#[
Enums in Nim are like enums in C, but are type-checked. There are no anonymous enums in Nim.
]#
type
  CompassDirections = enum
    cdNorth, cdEast, cdSouth, cdWest

  Colors {.pure.} = enum
    Red = "FF0000", Green = (1, "00FF00"), Blue = "0000FF"

  OtherColors {.pure.} = enum
    Red = 0xFF0000, Orange = 0xFFA500, Yellow = 0xFFFF00

  Signals = enum
    sigQuit = 3, sigAbort = 6, sigKill = 9

# echo Red  # Error: ambiguous identifier: 'Red'
echo Colors.Red
echo OtherColors.Red
echo OtherColors.Orange, " ", Orange

## Files
## - Reading from a File
#[
Suppose we have a file in the same directory as our nim program, kittens.txt with the following contents.
  #[
      Spitfire
      Vivian
      Motor
  ]#
We can use the [readFile] proc to read the entire file into memory.
# This will read the entire file into the string entireFile

## let entireFile = readFile("kittens.txt")
## echo entireFile  # prints the entire file

We can also read the lines of a file by opening a File object and using the readLine proc to read individual lines.

## proc readKittens() =
##  let f = open("kittens.txt")
##  # Close the file object when you are done with it
##  defer: f.close()

##  let firstLine = f.readLine()
##  echo firstLine  # prints Spitfire

## readKittens()

]#
## - Writing to a File
#[
We can write a string to a file using the [writeFile] proc.

let text = "Cats are very cool!"
writeFile("cats.txt", text)

This will create a file on the system named cats.txt containing “Cats are very cool!”

We can also write a file line by line using a File object and the writeLine proc.

proc writeCatActivities() =
  let lines = ["Play", "Eat", "Sleep"]
  # The fmWrite constant specifies that we are opening the file for writing.
  let f = open("catactivities.txt", fmWrite)
  defer: f.close()

  for line in lines:
    f.writeLine(line)

writeCatActivities()

After running this program there should be a file called catactivities.txt with the following contents.

Play
Eat
Sleep
]#
