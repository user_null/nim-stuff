import unittest
import std/math
## FIBONACCI SEQUENCE
## Analytic

func FibonacciANALYTIC(n: int): int64  {.exportc: "fibAnalytic" }=
  if n <= 0:
    return 0
  if n <= 2:
    return 1
  let fn = float64(n)
  const p: float64 = (1.0 + sqrt(5.0)) / 2.0
  const q: float64 = 1.0 / p
  let r: float64 = round((pow(p, fn) + pow(q, fn)) / sqrt(5.0))
  int64(r)

## Iterative

func FibonacciITERATIVE(n: int): int  {.exportc: "fibIterative" }=
  if n <= 0:
    return 0
  if n <= 2:
    return 1
  var
    result = 0
    second = 1
  for i in 0 .. n - 1:
    swap result, second
    second += result
  result

## Recursive

func FibonacciRECURSIVE(n: int): int64  {.exportc: "fibRecursive" }=
  if n <= 0:
    return 0
  if n <= 2:
    return 1
  FibonacciRECURSIVE(n - 1) + FibonacciRECURSIVE(n - 2)

## Tail-recursive

func FibonacciTAIL(n: int, current: int64, next: int64): int64 =
  if n <= 0:
    return current
  FibonacciTAIL(n - 1, next, current + next)

func FibonacciTAIL(n: int): int64 {.exportc: "fibTail" } =
  FibonacciTAIL(n, 0, 1)



## FACTORIAL
## Recursive
func FactorialRECURSIVE(x:int): int {.exportc: "factRecursive" }=
  if not(x > 0): return 1
  x * FactorialRECURSIVE(x - 1)

## Iterative
func FactorialITERATIVE(x: int): int {.exportc: "factIterative"} =
  result = 1
  for i in 2..x:
    result *= i


when isMainModule:
  suite "Fibonacci":
    test "WHEN input is 0 THEN the output is 0":
      check FibonacciANALYTIC(0) == 0
      check FibonacciITERATIVE(0) == 0
      check FibonacciRECURSIVE(0) == 0
      check FibonacciTAIL(0) == 0
    test "WHEN input is 1 THEN the output is 1":
      check FibonacciANALYTIC(1) == 1
      check FibonacciITERATIVE(1) == 1
      check FibonacciRECURSIVE(1) == 1
      check FibonacciTAIL(1) == 1
    test "WHEN input is 2 THEN the output is 1":
      check FibonacciANALYTIC(2) == 1
      check FibonacciITERATIVE(2) == 1
      check FibonacciRECURSIVE(2) == 1
      check FibonacciTAIL(2) == 1
    test "WHEN input is -1 THEN the output is 0":
      check FibonacciANALYTIC(-1) == 0
      check FibonacciITERATIVE(-1) == 0
      check FibonacciRECURSIVE(-1) == 0
      check FibonacciTAIL(-1) == 0
  suite "Factorial":
    test "WHEN input is 0 THEN the output is 1":
      check FactorialRECURSIVE(0) == 1
      check FactorialITERATIVE(0) == 1
    test "WHEN input is 1 THEN the output is 1":
      check FactorialRECURSIVE(1) == 1
      check FactorialITERATIVE(1) == 1
    test "WHEN input is 2 THEN the output is 2":
      check FactorialRECURSIVE(2) == 2
      check FactorialITERATIVE(2) == 2
    test "WHEN input is 3 THEN the output is 6":
      check FactorialRECURSIVE(3) == 6
      check FactorialITERATIVE(3) == 6
    test "WHEN input is -1 THEN the output is 1":
      check FactorialRECURSIVE(-1) == 1
      check FactorialITERATIVE(-1) == 1
