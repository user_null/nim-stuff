import unittest
import strutils
## Procedures
##  - Take inputs
##  - - Separate in set flags and mail input
##  - Convert mail input to undercase
##  - Select mail template
##  - - Select lang?
##  - - Using a flag?
##  - Use the template
##  - - Replace the email in the place of a placeholder
##  - Outuput it and open
##  - - Select with a flag if HTML or plaintext on the terminal
## Types
##  - Email  :: String
##  - Template :: String
## Wishlist
## - parseStdInput
## - openTemplate
## - - replaceTemplate

func replacePlaceholder(templateTxt:string,
                        placeholderTxt:string,
                        placeholderShape:string):string =
    replaceWord(templateTxt,"<" & placeholderShape & ">",placeholderTxt)

when isMainModule:
  suite "Template handling":
    suite "Replace Placeholder in Template":
      test "When receive empty string return it as is":
        check replacePlaceholder(templateTxt="",
                                 placeholderTxt="",
                                 placeholderShape="") == ""
      test "Replace the placeholder":
        check replacePlaceholder(templateTxt="<>",
                                 placeholderTxt="",
                                 placeholderShape="") == ""
      test "Make sure final functionality works, replace a placeholder with the correspondent placeholder text input":
        check replacePlaceholder(templateTxt="The mail that have input is: <email>",
                                 placeholderTxt="email@test.com",
                                 placeholderShape="email") == "The mail that have input is: email@test.com"
        check replacePlaceholder(templateTxt="The password that have input is: <password>",
                                 placeholderTxt="password123",
                                 placeholderShape="password") == "The password that have input is: password123"
        check replacePlaceholder("The input is: <text>","asdf","text") == "The input is: asdf"
